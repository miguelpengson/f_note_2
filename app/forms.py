from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired

class Notes(FlaskForm):
    name = StringField('Names', validators=[DataRequired()] )
    submit = SubmitField('Save')
    content = TextAreaField('Content', validators=[DataRequired()])