from flask import render_template, request, url_for, flash, redirect
from app import app
from app.forms import Notes


@app.route("/")
@app.route("/index", methods=['GET', 'POST'] )
def index():
    form = Notes()
    if form.validate_on_submit():
        return redirect(url_for('success'))
    return render_template('index.html', title='Name', form=form)